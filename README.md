# PEGGLE DATA COLLECTOR

## If you don't know what's peggle why are you even here xD

one day i am gonna take good care of this ;-;

## TODO / v2.0

---

things i wanna implement:

- hash all the stored passwords
- add jwt for user authentication
- add OAuth2.0 (github OAuth seems the best option so far)
- add full search algorithm (mongob atlas offer that but means migrate to atlas)
- let user query a date like 2021-2-4 and convert it to 2021-02-04
- check if a structure has default values (means not modified)

todo for this version:

- add function that let user clean just a section of the game adding function and not only everything
- section to add a comment while inserting a game
- dont create a button in commit area if there are none
- colors on buttons in add-game ✔️
- user section ✔️
- users section
- stats database
- main page
  - home with all the basic general statistics
  - individual game page with all the stats of the game
  - players stats (use stats databaes) (scatter graph, bar graph, pie graph)
  - api documentation area
  - support area (my paypal or buymeacoffe)
  - story of the idea (why am i doing this and effort)
