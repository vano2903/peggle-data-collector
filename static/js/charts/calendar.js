google.charts.load("current", { packages: ["calendar"] });
google.charts.setOnLoadCallback(calendar);

var years
var commits
var delay = 250
var throttled = false

async function getUsersCommitsYears(user) {
    var res = await fetch('/commit/years', {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    });
    var resp = await res.text();
    var years = resp.split(";");
    console.log(years);
    return years
}

async function genCalendarCommits(user, year) {
    user.year = parseInt(year);
    var res = await fetch('/commit/year', {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    });
    var respJson = await res.json();
    console.log(respJson);
    var dataSet = [];

    respJson.forEach(function (item) {
        dataSet.push([new Date(item.date), item.totals])
    });

    return dataSet;
}

function drawCommitChart(opt, dataSet) {
    var dataTable = new google.visualization.DataTable();
    dataTable.addColumn({ type: 'date', id: 'Date' });
    dataTable.addColumn({ type: 'number', id: 'Won/Loss' });
    dataTable.addRows(dataSet);

    var chart = new google.visualization.Calendar(document.getElementById('commits-graph'));

    chart.draw(dataTable, opt);
}

async function getTotalCommits(user) {
    var res = await fetch('/commit/totCommits', {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    });
    var resp = await res.text();
    var respJson = JSON.parse(resp)
    console.log(respJson.totalCommits)
    return respJson.totalCommits
}

async function calendar() {
    var width = $(window).width();
    var calendarOptions = genCalendarOptions(width, user.username)

    if (commits == undefined) {
        commits = await genCalendarCommits(user, years[years.length - 1])
    }

    drawCommitChart(calendarOptions, commits);
}

function drawYearsButtons(years) {
    var buttonContainer = document.getElementById("calendar-buttons");
    buttonContainer.innerHTML = "";
    years.forEach(function (item) {
        var but = document.createElement("button");
        but.innerHTML = item;
        but.value = item;
        but.className = "btn btn-primary";
        but.addEventListener('click', async function (e) {
            commits = await genCalendarCommits(user, item)
            calendar()
        }, false);
        buttonContainer.appendChild(but);
        buttonContainer.appendChild(document.createElement("br"));
        buttonContainer.appendChild(document.createElement("br"));
    });
}

window.addEventListener('resize', function () {
    if (!throttled) {
        if (locationSaver() == "stats") {
            calendar()
        }
        throttled = true;
        setTimeout(function () {
            throttled = false;
        }, delay);
    }
});

async function initCalendar() {
    years = await getUsersCommitsYears(user);
    drawYearsButtons(years)
    calendar()
    document.getElementById("total-commits").innerHTML = "numero totale di contributi: " + await getTotalCommits(user)
}

$(document).ready(async function () {
    if (locationSaver() == "stats") {
        await initCalendar()
    }
})

function genCalendarOptions(width, name) {
    var opt = {}
    opt.title = "Commits di " + name;
    opt.height = 250;

    var calendar = {};
    if (width < 500) {
        calendar.cellSize = 5;
    } else if (width < 600) {
        calendar.cellSize = 8;
    } else if (width < 800) {
        calendar.cellSize = 11;
    } else if (width < 1000) {
        calendar.cellSize = 12;
    } else if (width < 1200) {
        calendar.cellSize = 16;
    } else {
        calendar.cellSize = 19;
    }

    opt.calendar = Object.assign(calendar)
    return opt
}